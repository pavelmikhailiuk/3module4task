package facade.handler.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import facade.dao.PersonOutputStream;
import facade.dao.impl.PersonOutputStreamImpl;
import facade.domain.Person;
import facade.handler.PersonHandler;

public class PersonHandlerImpl implements PersonHandler {

	private String fileName;

	public PersonHandlerImpl(String fileName) {
		this.fileName = fileName;
	}

	public int whoIsSmarter(Person person1, Person person2) {
		return (person1.getIq() - person2.getIq()) > 0 ? 1 : 2;
	}

	public void moveIq(Person person1, Person person2, int iqToMove) {
		person1.setIq(person1.getIq() - iqToMove);
		person2.setIq(person2.getIq() + iqToMove);
		saveChanges(person1);
		saveChanges(person2);
	}

	public void changeIq(Person person, int iqChange) {
		person.setIq(person.getIq() + iqChange);
		saveChanges(person);
	}

	private void saveChanges(Person person) {
		PersonOutputStream personOutputStream = null;
		try {
			personOutputStream = new PersonOutputStreamImpl(new FileOutputStream(new File(fileName), true));
			personOutputStream.writePerson(person);
		} catch (IOException e) {
			System.err.println(e);
		} finally {
			try {
				if (personOutputStream != null) {
					personOutputStream.close();
				}
			} catch (IOException e) {
				System.err.println(e);
			}
		}
	}
}
