package facade.handler;

import facade.domain.Person;

public interface PersonHandler {

	int whoIsSmarter(Person person1, Person person2);

	void moveIq(Person person1, Person person2, int iqToMove);

	void changeIq(Person person, int iqChange);

}
