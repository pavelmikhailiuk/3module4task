package facade.dao;

import java.io.IOException;

import facade.domain.Person;

public interface PersonOutputStream {

	void writePerson(Person person) throws IOException;

	void close() throws IOException;
}
