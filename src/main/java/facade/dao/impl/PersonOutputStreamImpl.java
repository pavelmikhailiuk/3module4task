package facade.dao.impl;

import java.io.IOException;
import java.io.OutputStream;

import facade.dao.PersonOutputStream;
import facade.domain.Person;

public class PersonOutputStreamImpl implements PersonOutputStream {

	private OutputStream outputStream;

	public PersonOutputStreamImpl(OutputStream outputStream) {
		this.outputStream = outputStream;
	}

	public void writePerson(Person person) throws IOException {
		outputStream.write(person.toString().getBytes());
		outputStream.write(",".getBytes());
	}

	public void close() throws IOException {
		outputStream.close();
	}
}
