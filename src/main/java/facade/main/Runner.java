package facade.main;

import facade.domain.Person;
import facade.handler.PersonHandler;
import facade.handler.impl.PersonHandlerImpl;

public class Runner {
	public static void main(String[] args) {
		Person person1 = new Person("John", 20, 70);
		Person person2 = new Person("Jack", 30, 100);
		PersonHandler personHandler = new PersonHandlerImpl("persons.txt");
		System.out.println("Smartest person is " + personHandler.whoIsSmarter(person1, person2));
		personHandler.moveIq(person1, person2, -15);
		personHandler.changeIq(person1, 20);
	}
}
